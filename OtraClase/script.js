const inputUsuario = document.getElementById("username")
// inputUsuario.onclick = click
// inputUsuario.onmouseover = overMouse

function clicks(){
    const arts = document.getElementById("formulario")
    const parr = document.createElement("p")
    parr.innerHTML = "Esto fue agregado al hacer clic2"

    arts.appendChild(parr)
    inputUsuario.removeEventListener("click",clicks)
}

function overMouse(){
    const arts = document.getElementById("formulario")
    const parr = document.createElement("p")
    parr.innerHTML = "El mouse ha pasado por encima"

    arts.appendChild(parr)
    inputUsuario.removeEventListener("mouseover", overMouse)

}


//Standarizado
//Sin "ON"
inputUsuario.addEventListener("click", clicks)
inputUsuario.addEventListener("mouseover", overMouse)
