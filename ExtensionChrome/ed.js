function hideElement(ev){
    const element = ev.target
    element.style.display = "none"
    document.body.style.overflow = "auto"
}

function addBorder(ev){
    const element = ev.target
    element.style.border = "2px solid tomato"
}

function removeBorder(ev){
    const element = ev.target
    element.style.border = ""
}

document.body.addEventListener("click", hideElement)
document.body.addEventListener("mouseover", addBorder)
document.body.addEventListener("mouseout", removeBorder)

const modal = document.createElement("div")

function openModal (){
    document.body.style = "overflow: hidden"
    modal.classList.add("show")
    modal.innerHTML = `<h2>VUELVE AL TRABAJO</h2><p>Termino tu tiempo</p>`
    document.body.appendChild(modal)
}

setTimeout(openModal, 10000)
